The entity browser widget lacks the possibility of flagging the field widget in the form
if an error occurs. This module flags the entity browser widget on an error just as Drupal
core does it for other fields (input, selects, textareas), by highlighting the element
in red.

Since this is basically done by adding a class "error" to the field widget (just as core does),
the styling can easily be changed.

Imho this module provides a small feature that entity browser currently lacks, I would be glad
to see this functionality in entity browser. That would make this module obsolete, but in the
meantime this might be helpful.